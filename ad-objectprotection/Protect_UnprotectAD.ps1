﻿# Clear Screen
Clear-Host

# Another Change

# Deal with Errors
$ErrorActionPreference = "SilentlyContinue"

#  Make sure we load the AD Module
Import-Module ActiveDirectory

# Domain Distinguished name 
$Root = [ADSI]"LDAP://RootDSE" 
$ADPath = $Root.rootDomainNamingContext

#Set Variables
$Test = $False
$Message = $null

do {
    cls
    Write-Host -ForegroundColor Yellow "=============================================================================="
    Write-host -ForegroundColor white "Working in Domain "$ADPath
    Write-Host -ForegroundColor Yellow "=============================================================================="
    Write-Host
    Write-Host -ForegroundColor Yellow $Message
    Write-Host
    Write-Host -ForegroundColor Yellow "Please set the container to work with in Active Directory" 
    Write-Host
    Write-Host "OU to protect/Unprotect (format 'OU= or OU= ,OU=  or CN= ') - Leave Blank for the entire Domain"

    $ADOU = Read-Host ">" 

    if ([string]::IsNullOrWhiteSpace($ADOU)) {
        $ADOU = $null
        $YN = Read-Host "Entire Domain Selected, is this Correct? (Y/N)"
        if ($YN -eq "Y") {
            $Test = $True
            else {
                $Test = $False
                $Message = $null
            }
        }
    }

    elseif ($ADOU.substring(2, 1) -ne "=") {
        $Message = "Format Incorrect"
        $Test = $False
    }

    elseif (!([adsi]::Exists("LDAP://$adou,$adpath"))) {
        $Message = "OU Does Not Exist"
        $Test = $False

    }

    else {

        $Test = $True
    }
}

until ($Test)

if ($ADOU -eq $null) {
    $ADBase = $ADPath
}
else {
    $ADBase = $ADOU + "," + $ADPath
}

function Show-Menu {
    param (
        [string]$Title = 'Set AD Object Protection'
    )
    
    Write-Host -ForegroundColor Yellow "================ $Title =============================================================="
    Write-Host    
    Write-Host -ForegroundColor Yellow "1: "-NoNewline ; write-host -ForegroundColor White "Protect all AD Objects in $ADBase"
    Write-Host -ForegroundColor Yellow "2: "-NoNewline ; write-host -ForegroundColor White "UnProtect all AD Objects in $ADBase" 
    Write-Host -ForegroundColor Yellow "Q: "-NoNewline ; write-host -ForegroundColor White "Press 'Q' to quit."
    Write-Host
}

do {
    cls
    write-host -ForegroundColor Yellow "====================================================================================================="
    Write-host -ForegroundColor white " This will set the Set/Unset Protection on all of the AD objects in $ADBase"
    write-host -ForegroundColor Yellow "====================================================================================================="
    write-host

    Show-Menu
    $input = Read-Host "Please make a selection"
    switch ($input) {
        '1' {
            Get-ADObject -Filter * -SearchBase "$ADBase" | Set-ADObject -ProtectedFromAccidentalDeletion $true #-whatif
        } '2' {
            Get-ADObject -Filter * -SearchBase "$ADBase" | Set-ADObject -ProtectedFromAccidentalDeletion $false #-whatif
        } 'q' {
            return
        }
    }
    write-host
    "Verification: Please review pop up window"
    Get-ADObject -Filter * -SearchBase "$ADBase" -Properties "ProtectedFromAccidentalDeletion" | Out-GridView -OutputMode Single
    Write-host
    
     
}
until ($input -eq 'q')
