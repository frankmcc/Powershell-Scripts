﻿clear
$ErrorActionPreference = "silentlycontinue"

Import-Module ActiveDirectory

$computer = (Get-ADComputer -Filter 'ObjectClass -eq "Computer"' | Select -Expand DNSHostName)
foreach ($computer1 in $computer) {
$computer1
Invoke-GPUpdate -Computer $computer1 -Force
}