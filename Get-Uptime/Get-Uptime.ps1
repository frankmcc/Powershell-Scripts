﻿Import-Module ActiveDirectory

# Gimme a clean screan
Clear-Host
# Here is what to do if you encounter an error
$ErrorActionPreference = "silentlycontinue"

# Set number of days system has been up to flag.
$toolong=28

#Domains
$Domains = "domain.com"
# Domains - arrays are Ok here if you need more than one domain tested.
# $Domains = ("domainone.net"),("domain2.com")

# Leave this blank the variable will be populated later Just Declaring it now.
$DC = ""

function Show-Uptime {
# Get the work done

foreach ($computer1 in $computer) {

<# This command is the asy method of just getting your information, but I wanted to present it
in a way that was easier to read and flagged computers that have been up too long.

Get-Uptime -ComputerName $computer1

#>

<# This code does the same thing as Get-Uptime -ComputerName $computer1 only fancier
 #>

Get-WmiObject -Computername $computer1 -Class win32_operatingsystem -Property LastBootUpTime | Out-Null
$os = Get-WmiObject -Computername $computer1 -Class win32_operatingsystem 
$os.ConvertToDateTime($os.LastBootUpTime) | Out-Null
$uptime = (get-date) - $os.ConvertToDateTime($os.LastBootUpTime)

"---------------------------------------------------"
If ($uptime.Days -gt $toolong) {
Write-Host $computer1 -foregroundcolor Magenta -NoNewline
Write-Host " Overdue for Reboot" -ForeGroundColor Cyan
Write-host "UpTime: " -foregroundcolor Yellow -NoNewline
Write-host $uptime.Days "Days " -NoNewline
Write-host $uptime.Hours "Hours " -NoNewline
Write-host $uptime.Minutes "Minutes " 
}
else{
Write-Host $computer1 -foregroundcolor Green
Write-host "UpTime: " -foregroundcolor Yellow -NoNewline
Write-host $uptime.Days "Days " -NoNewline
Write-host $uptime.Hours "Hours " -NoNewline
Write-host $uptime.Minutes "Minutes "
}
}
}

Write-Host "Computers that have been up for over $toolong days will be flagged in " -NoNewline
Write-Host "magenta" -ForegroundColor Magenta
Write-Host 
Write-Host -ForegroundColor Yellow "You may see long delays while this script runs.  It will complete, have paitence..."


foreach ($Domain in $Domains) {
# Find the Domain Controller so your domain queries work
$DC = Get-ADDomainController -DomainName $Domain -Discover
write-host
write-host "=============================================" -ForegroundColor DarkCyan
write-host "     Domain:"$Domain -ForegroundColor White
write-host "         DC:"$DC -ForegroundColor White
write-host "=============================================" -ForegroundColor DarkCyan
write-host 


# Get a list of computers from Active Directory
$computer = (Get-ADComputer -Server $Domain -Filter { (OperatingSystem -like "*Server*") -and (Enabled -eq "True") } | Select-object -Expand DNSHostName)

Show-Uptime
}
Write-Host
Pause
Write-Host
#< Like this?  Checkout more at http://www.xpertnotes.net #>
