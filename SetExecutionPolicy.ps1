﻿
$url= "https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_execution_policies?view=powershell-6"

function Show-Menu
{
     param (
           [string]$Title = 'Set Execution Policy'
     )
    
     Write-Host -ForegroundColor Yellow "================ $Title ================"
     Write-Host    
     Write-Host -ForegroundColor Yellow "1: "-NoNewline ; write-host -ForegroundColor White "Restricted (Windows Client Computer Default)"
     Write-Host -ForegroundColor Yellow "2: "-NoNewline ; write-host -ForegroundColor green "Remote Signed (Most Common - Windows Server Default)" 
     Write-Host -ForegroundColor Yellow "3: "-NoNewline ; write-host -ForegroundColor White "All Signed (Less Secure)"
     Write-Host -ForegroundColor Yellow "4: "-NoNewline ; write-host -ForegroundColor DarkYellow "Bypass (Least Secure, use with extreme caution)"
     Write-Host -ForegroundColor Yellow "Q: "-NoNewline ; write-host -ForegroundColor White "Press 'Q' to quit."
     Write-Host
}

do
{
     Clear-Host
     $ExecPolicy = get-executionpolicy
     write-host -ForegroundColor Yellow "=============================================="
     Write-host -ForegroundColor white " Current Execution Policy is: " -NoNewline ; write-host -ForegroundColor magenta $ExecPolicy
     write-host -ForegroundColor Yellow "=============================================="
     write-host

     write-host "For details about Powershell Execution policies please visit:" 
     Write-Host -ForegroundColor cyan $URL 
     write-host 

     Show-Menu
     $input = Read-Host "Please make a selection"
     switch ($input)
     {
           '1' {
                Set-ExecutionPolicy undefined
           } '2' {
                Set-ExecutionPolicy RemoteSigned
           } '3' {
                Set-ExecutionPolicy AllSigned
           } '4' {
                Set-ExecutionPolicy ByPass
           } 'q' {
                return
           }
     }
     write-host
     write-host 'New Execution Policy is:' $ExecPolicy
     write-host
}
until ($input -eq 'q')
