Import-Module -Name webadministration

$FTPsite = 'IIS:\Sites\hollandcomputers.com'
$Thumbprint = (Get-ChildItem -path cert:\LocalMachine\My | Where-Object -Property Subject -eq "CN=hollandcomputers.com").Thumbprint

Set-ItemProperty -Path $FTPsite -Name ftpServer.security.ssl.serverCertHash -Value $Thumbprint



